import { Examen } from './../_model/examen';
import { HttpClient } from '@angular/common/http';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { Subject } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExamenService {

  ExamenCambio = new Subject<Examen[]>();
  mensaje = new Subject<string>();
  url: string = `${HOST}/examenes`;

  constructor(private http: HttpClient) { }

  listarExamenes(){
    return this.http.get<Examen[]>(this.url);
  }

  listarExamenPorId(id: number){
    return this.http.get<Examen>(`${this.url}/${id}`);
  }

  registrar(examen: Examen){
    return this.http.post(this.url, examen);
  }

  modificar(examen: Examen){
    return this.http.put(this.url, examen);
  }

  eliminar(id: number){
    return this.http.delete(`${this.url}/${id}`);
  }
}
